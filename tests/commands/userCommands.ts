import axios from 'axios';


const baseURL = "http://localhost:8080";
const basePath = '/auth';
const register = '/register';
const login = '/login';
const users = '/users/';

let cookie: any;

interface User {
    email: string,
    password: string,
    username?: string
}

export const registerAUser = async (email: string, password: string, username: string) => {
    const body: User = {
            email,
            password,
            username
    }

    const response = await axios.post(baseURL + basePath + register, body)
    expect(response.status).toBe(200);
    expect(response.data.username).toBe(username);
    expect(response.data.email).toBe(email);

}
export const loginUser = async (email: string, password: string, username: string) => {
    const body: User = {
            email,
            password,
            username
    }

    const response = await axios.post(baseURL + basePath + login, body)
    cookie = response.headers['set-cookie'];

    expect(response.status).toBe(200);
    expect(response.data.username).toBe(username)
    expect(response.data.email).toBe(email)

    console.log(response.data)

    return response;
}

export const deleteUser = async (id: string, email: string, username: string) => {
    const response = await axios.delete(baseURL + users + id,{
        headers: {
          'Cookie': cookie
        }
      })
    
    expect(response.status).toBe(200);
    expect(response.data._id).toBe(id)
    expect(response.data.username).toBe(username)
    expect(response.data.email).toBe(email)
    
    console.log(response.data)
}

export const updateUser = async (id: string, updatedUserName: string) => {
    const body = {
        username: updatedUserName
    }
    const response = await axios.patch(baseURL + users + id, body, {
        headers: {
          'Cookie': cookie
        }
      })
    
    expect(response.status).toBe(200);
    expect(response.data._id).toBe(id)
    expect(response.data.username).toBe(updatedUserName)
    
    console.log(response.data)
}

export const getUserById = async (id: string, email: string, username: string) => {
    console.log(cookie)
    const response = await axios.get(baseURL + users + id,{
        headers: {
          'Cookie': cookie
        }
    })

    expect(response.status).toBe(200);
    expect(response.data.username).toBe(username)
    expect(response.data.email).toBe(email)

    console.log(response.data)
}