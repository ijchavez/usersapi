import axios, { AxiosResponse } from 'axios';

export const getUserData = async (response: AxiosResponse<any, any>) => {
    return response.data;
}