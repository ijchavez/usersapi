import {describe, test} from '@jest/globals';
import { deleteUser, getUserById, loginUser, registerAUser, updateUser } from '../commands/userCommands';
import { getUserData } from '../commands/commands';
import { getRandomNumber } from '../utils/utilities';

describe('User Tests', () => {
    test('login user', async () => {
        const user =await loginUser("test@test.com", "123", "gerardo");
        const userData = await getUserData(user);
        await getUserById(userData._id, "test@test.com", "gerardo")
    });
    test('register user and delete it', async () => {
        await registerAUser("gerardo.chavez@mail.com", "password", "gerardo1234");
        const user = await loginUser("gerardo.chavez@mail.com", "password", "gerardo1234");
        const userData = await getUserData(user);
        
        await deleteUser(userData._id, "gerardo.chavez@mail.com", "gerardo1234");
    });
    test('update user', async () => {
        const user = await loginUser("test@test.com", "123", "gerardo");
        const userData = await getUserData(user);
        
        await updateUser(userData._id, "gerardo " + getRandomNumber())
        await updateUser(userData._id, "gerardo")
    });
});