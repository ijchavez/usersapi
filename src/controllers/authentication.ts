import express from 'express';

import { createUser, getUserByEmail } from '../db/users.js';
import { authentication, random } from '../helpers/index.js';

export const login = async (req: express.Request, res: express.Response) => {
    try {
        const { email, password } = req.body;
        if(!email || !password) {
            return sendStatus(res, 400)
        }

        const user = await getUserByEmail(email).select('+authentication.salt +authentication.password');
        if(!user){
            return sendStatus(res, 400)
        }

        const expectedHash = authentication(user.authentication.salt, password)
        if(user.authentication.password !== expectedHash){
            return sendStatus(res, 403)
        }

        const salt = random();
        user.authentication.sessionToken = authentication(salt, user._id.toString());

        await user.save();
        res.cookie('GERARDO-AUTH', user.authentication.sessionToken, { path: '/' });
        
        return res.status(200).json(user).end()
    } catch (error) {
        console.log(error);
        return sendStatus(res, 404)
    }

}


export const register = async (req: express.Request, res: express.Response) => {
    try {
        const { email, password, username } = req.body;

        if(!email || !password || !username) {
            return sendStatus(res, 400)

        }
        const existingUser = await getUserByEmail(email)
        if(existingUser){
            return sendStatus(res, 400)

        }
        const salt = random();
        const user = await createUser({
            email,
            username,
            authentication: {
                salt,
                password: authentication(salt, password)
            }
        })
        return res.status(200).json(user).end()

    } catch (error) {
        console.log(error);
        return sendStatus(res, 404)
    }

}

const sendStatus = (res: express.Response, status:number) => {
    return res.sendStatus(status);
}