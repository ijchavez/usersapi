import express from 'express';

import { getAllUsers, deleteUser, updateUser, getUserByUserId } from '../controllers/users.js';
import { isAuthenticated, isOwner } from '../middlewares/index.js';



export default (router: express.Router) => {
    router.get('/users', isAuthenticated, getAllUsers);
    router.get('/users/:id', isAuthenticated, getUserByUserId);
    router.delete('/users/:id', isAuthenticated, isOwner, deleteUser);
    router.patch('/users/:id', isAuthenticated, isOwner, updateUser);
}